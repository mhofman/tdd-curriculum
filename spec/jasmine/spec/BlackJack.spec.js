describe('BlackJack Hand', function () {

    beforeEach(function () {
        this.hand1 = new BlackJack();
    });

    it('takes a card and shows you the score until you\'re bust', function () {
        expect(this.hand1.take('A', '9')).toEqual('20');
        expect(this.hand1.take('A')).toEqual('21');
        expect(this.hand1.take('K')).toEqual('21');
        expect(this.hand1.take('K')).toEqual('Bust');
    });

    it('relates the right value to ace', function () {
        expect(this.hand1.take('A')).toEqual('11');
        expect(this.hand1.take('A')).toEqual('12');
        expect(this.hand1.take('10')).toEqual('12');
    });

    it('recognizes Black Jack', function () {
        expect(this.hand1.take('A')).toEqual('11');
        expect(this.hand1.take('K')).toEqual('Black Jack');
    });

    it('recognizes Black Jack', function () {
        expect(this.hand1.take('10', 'A')).toEqual('Black Jack');
    });

    it('ignores rubbish', function () {
        expect(this.hand1.take('@', '#', '@')).toEqual('0');
    });

//    describe('isMobile', function () {
//        it('returns true if window.isDesktop is set', function () {
//            window.isMobile = true;
//            expect(this.hand1.isMobile()).toBeTruthy();
//        });
//        it('returns true if window.isMobile is set', function () {
//            window.isMobile = true;
//            expect(this.hand1.isMobile()).toBeTruthy();
//        });
//        it('returns true if window.bla equals Opera', function () {
//            window.bla = 'Opera';
//            expect(this.hand1.isMobile()).toBeTruthy();
//
//        });
//    });

});
