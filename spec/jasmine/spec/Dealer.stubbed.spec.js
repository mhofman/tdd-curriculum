describe('Dealer - stubbed', function () {


    describe('constructor', function () {

        beforeEach(function () {
            spyOn(Dealer2.prototype, 'shuffle').and.callFake(function (fake) {
                return fake;
            });
            this.dealer = new Dealer2();
        });

        it('creates a deck of 52 cards', function () {
            expect(this.dealer.deck.length).toEqual(52);
        });
        it('shuffles the deck', function () {
            expect(this.dealer.shuffle).toHaveBeenCalledWith(["♠A", "♠K", "♠Q", "♠J", "♠10", "♠9", "♠8", "♠7", "♠6", "♠5", "♠4", "♠3", "♠2", "♥A", "♥K", "♥Q", "♥J", "♥10", "♥9", "♥8", "♥7", "♥6", "♥5", "♥4", "♥3", "♥2", "♦A", "♦K", "♦Q", "♦J", "♦10", "♦9", "♦8", "♦7", "♦6", "♦5", "♦4", "♦3", "♦2", "♣A", "♣K", "♣Q", "♣J", "♣10", "♣9", "♣8", "♣7", "♣6", "♣5", "♣4", "♣3", "♣2"]);
        });
    });

    describe('shuffle', function () {

        beforeEach(function () {
            this.dealer = new Dealer2();
        });

        it('takes an array of two items, and returns an array with the same contents but in a different order', function () {
            expect(this.dealer.shuffle([1, 2])).toEqual([2, 1]);
        });

        it('takes an array of three items, and returns an array with the same contents but in a different order', function () {
            expect(this.dealer.shuffle([1, 2, 3])).not.toEqual([1, 2, 3]);
        });

        it('takes an array of four items, and returns an array with the same contents but in a different order', function () {
            expect(this.dealer.shuffle([1, 2, 3, 4])).not.toEqual([1, 2, 3, 4]);
        });

    });


    describe("hitMe", function () {

        beforeEach(function () {
            this.dealer = new Dealer2();
            this.dealer.deck = ['bottom', 'top'];
        });

        it("if the score is less than 21, Black Jack or Bust it gives the top card of the deck", function () {
            expect(this.dealer.hitMe('0')).toEqual('top');
        });

        it("removes the top card from the deck", function () {
            this.dealer.hitMe('0')
            expect(this.dealer.deck).toEqual(['bottom']);
        });

        it("asks the player\'s score when it doesn\'t provide it", function () {

            expect(this.dealer.hitMe()).toEqual('Please share your current score with me');

        });

        it("refuses to give a card to a player that has the highest score already", function () {

            expect(this.dealer.hitMe('21')).toEqual('You already have the highest score');
            expect(this.dealer.hitMe('Black Jack')).toEqual('You already have the highest score');


        });

        it("tells player it is bust when it is", function () {

            expect(this.dealer.hitMe('Bust')).toEqual('You\'re bust, I can\'t give you another card');

        });

    });

});