describe('BlackJack3', function () {

    beforeEach(function () {
        jasmine.getFixtures().fixturesPath = 'spec/fixtures';
        this.node = setFixtures('<div class="black-jack-hand"></div>');
//        this.fakeNode = loadFixtures('black-jack.html');
        this.result = setFixtures('<div class="black-jack-hand">' +
            '<div class="card ♠A"></div>' +
            '<div class="card ♠K"></div>' +
            '</div>');

        this.hand1 = new BlackJack3(this.node);
        spyOn(this.hand1, 'tellScore').and.callThrough();
    });

    describe('constructor', function () {

        it('stores the root node in the local variable', function () {
            expect(this.hand1.node).toEqual(this.node);
        });

    });

    describe('updateView', function () {

        it('takes a card and adds it to the dom node', function () {
            this.hand1.updateView('♠A');
            this.hand1.updateView('♠K');
            expect(this.hand1.node.html()).toEqual(this.result.html());
        });

    });

    describe('DOM click on node', function () {
        it('tells the score of the cards', function () {
            this.node.trigger('click');
            expect(this.hand1.tellScore).toHaveBeenCalled();
        });
    });

    describe('calculateScore', function () {

        it('looks at the hand and returns 20 if it contains ♠A and ♦9', function () {
            this.hand1.hand = ['♠A', '♦9'];
            expect(this.hand1.calculateScore()).toEqual('20');
        });

        it('returns 19 on ♠J and ♦9', function () {
            this.hand1.hand = ['♠J', '♦9'];
            expect(this.hand1.calculateScore()).toEqual('19');
        });

        it('returns 12 on ♠A and ♦A', function () {
            this.hand1.hand = ['♠A', '♦A'];
            expect(this.hand1.calculateScore()).toEqual('12');
        });

        it('returns Black Jack on ♠A and ♦K', function () {
            this.hand1.hand = ['♠A', '♦K'];
            expect(this.hand1.calculateScore()).toEqual('Black Jack');
        });

        it('returns bust on ♠5, ♦9 and ♣8', function () {
            this.hand1.hand = ['♠5', '♦9', '♣8'];
            expect(this.hand1.calculateScore()).toEqual('Bust');
        });

    });

    describe('take', function () {

        beforeEach(function () {

            this.hand1 = new BlackJack3(this.node);
            spyOn(this.hand1, 'calculateScore').and.returnValue('result');
            spyOn(this.hand1, 'updateView').and.callFake(function (card) {
            });



        });

        it('updates the view', function () {
            this.hand1.take('♣A');
            expect(this.hand1.updateView).toHaveBeenCalledWith('♣A');
        });

        it('takes a card and stores it in the hand', function () {
            this.hand1.take('♣A');
            expect(this.hand1.hand).toEqual(['♣A']);
        });

        it('calls calculateScore and returns its return value', function () {
            expect(this.hand1.take('♠5', '♦9', '♣8')).toEqual('result');
            expect(this.hand1.calculateScore).toHaveBeenCalled();
        });

    });


});
