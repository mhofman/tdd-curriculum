describe( "Score", function() {

    it( "scores 250 on 5 1 3 4 1", function() {
        expect( score( [5, 1, 3, 4, 1]) ).toEqual(250);
    });

    it( "scores 1100 on 1 1 1 3 1", function() {
        expect( score( [1, 1, 1, 3, 1]) ).toEqual(1100);
    });

    it( "scores 450 on 2 4 4 5 4", function() {
        expect( score( [2, 4, 4, 5, 4]) ).toEqual(450);
    });

    it( "scores 'cheater' on 6 6 6 5 5 5", function() {
        expect( score( [6, 6, 6, 5, 5, 5]) ).toEqual('cheater');
    });

});
