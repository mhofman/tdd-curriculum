describe('Dealer', function () {

    beforeEach(function () {
        this.dealer = new Dealer2();
    });

    it("has 52 cards", function () {
        expect(this.dealer.deck.length).toEqual(52);
    });
    it("shuffled the cards", function () {
        expect(this.dealer.deck.join(";")).not.toEqual("♠A;♠K;♠Q;♠J;♠10;♠9;♠8;♠7;♠6;♠5;♠4;♠3;♠2;♥A;♥K;♥Q;♥J;♥10;♥9;♥8;♥7;♥6;♥5;♥4;♥3;♥2;♦A;♦K;♦Q;♦J;♦10;♦9;♦8;♦7;♦6;♦5;♦4;♦3;♦2;♣A;♣K;♣Q;♣J;♣10;♣9;♣8;♣7;♣6;♣5;♣4;♣3;♣2");
    });

    describe("hitMe", function () {

        it("will give the top of the deck if the score allows it", function () {

            expect(this.dealer.hitMe('0')).toMatch(/^[♠♥♦♣]([AKQJ2-9]|10)$/);
            var topOfDeck = this.dealer.deck[this.dealer.deck.length - 1];
            expect(this.dealer.hitMe('0')).toEqual(topOfDeck);
            expect(this.dealer.deck.length).toEqual(50);

        });

        it("asks the player\'s score when it doesn\'t provide it", function () {

            expect(this.dealer.hitMe()).toEqual('Please share your current score with me');

        });

        it("refuses to give a card to a player that has the highest score already", function () {

            expect(this.dealer.hitMe('21')).toEqual('You already have the highest score');
            expect(this.dealer.hitMe('Black Jack')).toEqual('You already have the highest score');


        });

        it("tells player it is bust when it is", function () {

            expect(this.dealer.hitMe('Bust')).toEqual('You\'re bust, I can\'t give you another card');

        });

    });

});