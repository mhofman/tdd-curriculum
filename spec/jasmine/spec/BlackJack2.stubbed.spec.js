describe('BlackJack2', function () {

    beforeEach(function () {
        this.hand1 = new BlackJack2();
    });

    describe('calculateScore', function () { //Methid

        it('should return Black Jack when you have an Ace and a card with a value of 10', function () {
            this.hand1.hand = ['♠A', '♦9'];
            expect(this.hand1.calculateScore()).toEqual('20');
        });

        it('should return Black Jack when you have an Ace and a card with a value of 10', function () {
            // Given the hand has an Ace of Spades and a King of Diamonds
            this.hand1.hand = ['♠A', '♦K'];
            // When I call calculateScore
            // Then I expect the return value to be Black Jack
            expect(this.hand1.calculateScore()).toEqual('Black Jack');

            this.hand1.hand = ['♠A', '♦Q'];
            expect(this.hand1.calculateScore()).toEqual('Black Jack');

            this.hand1.hand = ['♠A', '♦J'];
            expect(this.hand1.calculateScore()).toEqual('Black Jack');

            this.hand1.hand = ['♠A', '♦10'];
            expect(this.hand1.calculateScore()).toEqual('Black Jack');
        });

        it('returns 12 on ♠A and ♦A', function () {
            this.hand1.hand = ['♠A', '♦A'];
            expect(this.hand1.calculateScore()).toEqual('12');
        });

        it('returns Black Jack on ♠A and ♦K', function () {
            this.hand1.hand = ['♠A', '♦K'];
            expect(this.hand1.calculateScore()).toEqual('Black Jack');
        });

        it('returns bust on ♠5, ♦9 and ♣8', function () {
            this.hand1.hand = ['♠5', '♦9', '♣8'];
            expect(this.hand1.calculateScore()).toEqual('Bust');
        });

    });

    describe('take', function () {

        beforeEach(function () {

            this.hand1 = new BlackJack2();
            spyOn(this.hand1, 'calculateScore').and.returnValue('Bust');
//            spyOn(this.hand1, 'calculateScore').and.callThrough();
        });

        it('takes a card and stores it in the hand', function () {
            this.hand1.take('♣A');
            expect(this.hand1.hand).toEqual(['♣A']);
        });

        it('takes two cards and stores it in the hand', function () {
            this.hand1.take('♣A', 'SK');
            expect(this.hand1.hand).toEqual(['♣A', 'SK']);
        });

        it('calls calculateScore and returns its return value', function () {
            expect(this.hand1.take()).toEqual('Bust');
            expect(this.hand1.calculateScore).toHaveBeenCalled();
        });

    });


});
