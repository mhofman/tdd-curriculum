var Dealer2 = function () {

    this.deck = "♠A;♠K;♠Q;♠J;♠10;♠9;♠8;♠7;♠6;♠5;♠4;♠3;♠2;♥A;♥K;♥Q;♥J;♥10;♥9;♥8;♥7;♥6;♥5;♥4;♥3;♥2;♦A;♦K;♦Q;♦J;♦10;♦9;♦8;♦7;♦6;♦5;♦4;♦3;♦2;♣A;♣K;♣Q;♣J;♣10;♣9;♣8;♣7;♣6;♣5;♣4;♣3;♣2".split(";");
    this.deck = this.shuffle(this.deck);
};

Dealer2.prototype = {
    shuffle: function shuffle(input) {
        var order = input.join();
        for (var i = input.length - 1; i >= 0; i--) {

            var randomIndex = Math.floor(Math.random() * (i + 1));
            var itemAtIndex = input[randomIndex];

            input[randomIndex] = input[i];
            input[i] = itemAtIndex;
        }
        if (input.join() === order) {
            this.shuffle(input);
        }
        return input;
    },
    hitMe: function (score) {
        switch (score) {
            case 'Bust':
                return 'You\'re bust, I can\'t give you another card';
            case 'Black Jack':
            case '21':
                return 'You already have the highest score';
            case undefined:
                return 'Please share your current score with me';
            default:
                return this.deck.pop();
        }
    }
};