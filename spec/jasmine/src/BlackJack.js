var BlackJack = function () {
    this.hand = [];
    this.scores = {
        A: 1,
        K: 10,
        Q: 10,
        J: 10,
        10: 10,
        9: 9,
        8: 8,
        7: 7,
        6: 6,
        5: 5,
        4: 4,
        3: 3,
        2: 2
    };
};

BlackJack.prototype = {
    take: function () {
        var i;
        for (i = 0; i < arguments.length; i++) {
            this.hand.push(arguments[i]);
        }
        return this.calculateScore();
    },
    calculateScore: function () {
        var that = this;
        var score = 0;
        var aces = 0;
        this.hand.forEach(function (card) {
            score += that.scores[card] || 0;
            if (card === 'A') {
                aces++;
            }
        });

        while (score + 10 <= 21 && aces > 0) {
            score += 10;
            aces -= 1;
        }

        if (score > 21) {
            return 'Bust';
        }
        if (score === 21 && this.hand.length === 2) {
            return 'Black Jack';
        }
        return score + '';
    },
    isMobile: function () {
        return window.isDesktop || window.isMobile || window.bla === 'Opera';
    }
};