var chai = require('chai');
var expect = chai.expect;

var BlackJack = require('../src/BlackJack.js');

describe('BlackJack Hand', function () {

    beforeEach(function () {
        this.hand1 = new BlackJack();
    });

    it('takes a card and shows you the score until you\'re bust', function () {
        expect(this.hand1.take('A', '9')).to.equal('20');
        expect(this.hand1.take('A')).to.equal('21');
        expect(this.hand1.take('K')).to.equal('21');
        expect(this.hand1.take('K')).to.equal('Bust');
    });

    it('relates the right value to ace', function () {
        expect(this.hand1.take('A')).to.equal('11');
        expect(this.hand1.take('A')).to.equal('12');
        expect(this.hand1.take('10')).to.equal('12');
    });

    it('recognizes Black Jack', function () {
        expect(this.hand1.take('A')).to.equal('11');
        expect(this.hand1.take('K')).to.equal('Black Jack');
    });

    it('recognizes Black Jack', function () {
        expect(this.hand1.take('10', 'A')).to.equal('Black Jack');
    });

    it('ignores rubbish', function () {
        expect(this.hand1.take('@', '#', '@')).to.equal('0');
    });

});
