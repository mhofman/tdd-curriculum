var chai = require('chai');
var expect = chai.expect;

var Dealer = require('../src/Dealer.js');

describe('Dealer', function () {

    beforeEach(function () {
        this.dealer = new Dealer();
    });

    it("has 52 cards", function () {
        expect(this.dealer.deck).to.have.length(52);
    });
    it("shuffled the cards", function () {
        expect(this.dealer.deck.join(";")).not.to.equal("♠A;♠K;♠Q;♠J;♠10;♠9;♠8;♠7;♠6;♠5;♠4;♠3;♠2;♥A;♥K;♥Q;♥J;♥10;♥9;♥8;♥7;♥6;♥5;♥4;♥3;♥2;♦A;♦K;♦Q;♦J;♦10;♦9;♦8;♦7;♦6;♦5;♦4;♦3;♦2;♣A;♣K;♣Q;♣J;♣10;♣9;♣8;♣7;♣6;♣5;♣4;♣3;♣2");
    });

    it("will give the top card from the deck", function () {
        expect(this.dealer.hitMe()).to.match(/^[♠♥♦♣]([AKQJ2-9]|10)$/);
        var topOfDeck = this.dealer.deck[this.dealer.deck.length -1];
        expect(this.dealer.hitMe()).to.equal(topOfDeck);
        expect(this.dealer.deck).to.have.length(50);
    });

});