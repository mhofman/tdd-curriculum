var chai = require('chai');
var expect = chai.expect;

var BlackJack = require('../src/BlackJack.js');

describe('BlackJack Hand', function () {

    beforeEach(function () {
        this.hand1 = new BlackJack();
    });

    it('takes a card and shows you the score until you\'re bust', function () {
        expect(this.hand1.take('A', '9')).to.equal('20');
    });


});
