function score(dice) {
    console.time('score');
    if (dice.length > 5) return 'cheater';
    var threes = [0, 1000, 200, 300, 400, 500, 600],
        ones = [0, 100, 0, 0, 0, 50, 0],
        dices = {},
        sum = 0;

    dice.forEach(function(v) { dices[v] = dices[v] ? dices[v] + 1 : 1; });
    for (var i = 1; i < 7; ++i) {
        if (!dices[i]) {
            continue;
        }

        if (dices[i] >= 3) {
            sum += threes[i];
            dices[i] -= 3;
        }
        sum += ones[i]*dices[i];
    }
    console.timeEnd('score');
    return sum;
}

module.exports = score;
